This module contains the "Busy" record which allows CA clients to indicate completion. This notification procedure works with EPICS putNotify and ca_put_callback mechanism.
